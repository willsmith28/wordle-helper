#!/usr/bin/env python
import argparse
import operator
import re
import string
from collections import defaultdict

WORDS_PATH = "/usr/share/dict/words"


def get_wordle_words() -> list[str]:
    with open(WORDS_PATH) as file:
        return [
            stripped_word
            for word in file.readlines()
            if word.islower()
            and len(stripped_word := word.strip()) == 5
            and stripped_word.isalpha()
            and stripped_word.isascii()
        ]


def exclude_character_pattern(
    excluded_letters: str | None, exclude_letters_this_position: list[str]
):
    if excluded_letters is None and not exclude_letters_this_position:
        return "."

    if excluded_letters is None:
        excluded_letters = ""

    return f"[^{excluded_letters}{''.join(exclude_letters_this_position)}]"


def main(
    excluded_letters: str | None,
    included_letters: set[str],
    position_information: list[str],
    print_counts: bool = False,
):
    if any(
        re.match(r"[0-4][\+\-][a-z]", item) is None for item in position_information
    ):
        raise ValueError("Positional information not formatted correctly")

    if excluded_letters is not None:
        excluded_letters = "".join(set(excluded_letters))

    known_positions = {
        int(position): letter
        for position, sign, letter in position_information
        if sign == "+"
    }
    known_exclusion_positions = defaultdict(set)
    for position, sign, letter in position_information:
        if sign == "-":
            known_exclusion_positions[int(position)].add(letter)

    pattern = "".join(
        (
            known_positions[index]
            if index in known_positions
            else exclude_character_pattern(
                excluded_letters, known_exclusion_positions[index]
            )
        )
        for index in range(5)
    )
    words = [
        word
        for word in get_wordle_words()
        if re.match(pattern, word)
        and all(letter in word for letter in included_letters)
    ]
    print(f"regex used: {pattern}")
    for word in words:
        print(word)

    if print_counts:
        known_letters = included_letters.union(known_positions.values())
        letter_counts = {
            letter: count
            for letter in string.ascii_lowercase
            if letter not in known_letters
            and (count := sum(1 for word in words if letter in word)) > 0
        }
        print(f"unknown letter counts:")
        for letter, count in sorted(
            letter_counts.items(), key=operator.itemgetter(1), reverse=True
        ):
            print((letter, count))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="provide a list of possible wordle choices"
    )
    parser.add_argument(
        "exclude",
        nargs="?",
        default=None,
        help="List of letters which are not in the word",
    )
    parser.add_argument(
        "-i",
        "--include",
        help="List of letters which are in the word but position is unknown",
        default="",
    )
    parser.add_argument(
        "-p",
        "--position",
        nargs="*",
        default=[],
        help=(
            "Space seperated list of information about specific postions. "
            'must match the regex r"[0-4][+-][a-z]" '
            "1st charachter: index "
            "2nd charachter: include(+) or exclude(-) "
            "3rd charachter: letter"
        ),
    )
    parser.add_argument(
        "-c",
        "--counts",
        action="store_true",
        help="print the count of how many possible words the letters no information is known are in",
    )
    args = parser.parse_args()
    main(args.exclude, set(args.include), args.position, args.counts)
