.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://api.cirrus-ci.com/github/<USER>/wordle-helper.svg?branch=main
        :alt: Built Status
        :target: https://cirrus-ci.com/github/<USER>/wordle-helper
    .. image:: https://readthedocs.org/projects/wordle-helper/badge/?version=latest
        :alt: ReadTheDocs
        :target: https://wordle-helper.readthedocs.io/en/stable/
    .. image:: https://img.shields.io/coveralls/github/<USER>/wordle-helper/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/wordle-helper
    .. image:: https://img.shields.io/pypi/v/wordle-helper.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/wordle-helper/
    .. image:: https://img.shields.io/conda/vn/conda-forge/wordle-helper.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/wordle-helper
    .. image:: https://pepy.tech/badge/wordle-helper/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/wordle-helper
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/wordle-helper

.. image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
    :alt: Project generated with PyScaffold
    :target: https://pyscaffold.org/

|

=============
wordle-helper
=============


    Add a short description here!


A longer description of your project goes here...


.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.1.4. For details and usage
information on PyScaffold see https://pyscaffold.org/.
